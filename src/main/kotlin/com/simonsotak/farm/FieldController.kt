package com.simonsotak.farm

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("fields")
class FieldController {
    @Autowired
    lateinit var repository: FieldRepository

    @GetMapping("")
    fun findAll(): Iterable<Field> = repository.findAll()

    @PostMapping("")
    fun createField(@RequestBody newField: Field): Field = repository.save(newField)

    @PutMapping
    fun updateField(@RequestBody field: Field) = repository.save(field)

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long) = repository.findById(id)

    @DeleteMapping("/{id}")
    fun deleteField(@PathVariable id: Long) = repository.deleteById(id)
}
