package com.simonsotak.farm

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.validation.constraints.NotNull

@Entity
data class Farm(
    @NotNull
    var name: String = "",

    var note: String? = null,

    @OneToMany
    var fields: List<Field> = emptyList(),

    @Id @GeneratedValue
    val id: Long = -1)
