package com.simonsotak.farm

import com.vividsolutions.jts.geom.Polygon
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Field(
    @ManyToOne
    var farm: Farm? = null,

    @NotNull
    var name: String = "",

    var geometry: Polygon? = null,

    @Id @GeneratedValue
    val id: Long = -1
)
