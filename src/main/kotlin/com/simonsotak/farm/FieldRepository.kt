package com.simonsotak.farm

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FieldRepository : CrudRepository<Field, Long>
