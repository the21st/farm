package com.simonsotak.farm

import com.bedatadriven.jackson.datatype.jts.JtsModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JtsJacksonConfig{

    @Bean
    fun jtsModule() = JtsModule()
}
