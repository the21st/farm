package com.simonsotak.farm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FarmApplication

fun main(args: Array<String>) {
	runApplication<FarmApplication>(*args)
}
