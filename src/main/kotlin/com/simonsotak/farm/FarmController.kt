package com.simonsotak.farm

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("farms")
class FarmController {
    @Autowired
    lateinit var repository: FarmRepository

    @GetMapping("")
    fun findAll(): Iterable<Farm> = repository.findAll()

    @PostMapping("")
    fun createFarm(@RequestBody newFarm: Farm): Farm = repository.save(newFarm)

    @PutMapping
    fun updateFarm(@RequestBody farm: Farm) = repository.save(farm)

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long) = repository.findById(id)

    @DeleteMapping("/{id}")
    fun deleteFarm(@PathVariable id: Long) = repository.deleteById(id)
}
