package com.simonsotak.farm

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FarmRepository : CrudRepository<Farm, Long>
